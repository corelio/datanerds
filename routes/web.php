<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

// Authentication routes
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

// Application routes for authenticated users
Route::get('/home', 'HomeController@index');
Route::get('/commandeer', 'ShipController@commandeer')->middleware('auth');;
Route::post('/commandeer', 'ShipController@post');

// Add a pirate
Route::post('/pirate/add', 'PirateController@add');

// List Ports
Route::get('/ports', 'PortController@listPorts')->middleware('auth');;

// App route for attack
Route::get('/app/attack', 'PirateController@attack')->middleware('auth');;

// Ships
Route::group(['prefix' => 'ships'], function () {
    Route::get('/', 'ShipController@listShips')->middleware('auth');;
    Route::get('/edit', 'ShipController@editShip')->middleware('auth');;
    Route::post('/edit', 'ShipController@updateShip');
    Route::get('/near', function () { return view('shipsLocation');})->middleware('auth');;
    Route::post('/near', 'GeoShipController@checkAround');
});

// Treasure routes
Route::get('/treasure', 'TreasureController@index')->middleware('auth');;
Route::post('/treasure', 'TreasureController@useTreasure');
