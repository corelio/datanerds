<?php

use Illuminate\Database\Seeder;
use App\GeoShip;

class GeoShipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $ships = 100;

        for ($i = 0; $i < $ships; $i++) {
            $ship = new GeoShip();
            $ship->name = $faker->name;
            $ship->location = [$faker->longitude(-123, -170), $faker->latitude(37, 55)];
            $ship->save();
        }
    }
}
