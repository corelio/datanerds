<?php

use Illuminate\Database\Seeder;

class Ports2AttackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $ports = 10;

        for ($i = 0; $i < $ports; $i++) {
            DB::table('ports2attacks')->insert([
                'name' => $faker->city,
                'treasure_amount' => $faker->numberBetween(100, 99999),
            ]);
        }
    }
}
