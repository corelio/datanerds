<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ports2attack extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'attacked_at', 'treasure_amount'
    ];

    /**
     * Attacks belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public $timestamps = false;

}
