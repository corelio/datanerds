<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GeoShip extends Eloquent
{

    protected $connection = 'mongodb';

    protected $collection = 'ships';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id', 'name', 'location'
    ];


}
