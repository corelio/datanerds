<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', 'App\Http\ViewComposers\UserComposer');
        View::composer('*', 'App\Http\ViewComposers\Ports2AttackComposer');
        View::composer('*', 'App\Http\ViewComposers\TreasureComposer');
        View::composer('*', 'App\Http\ViewComposers\AttackComposer');
        View::composer('*', 'App\Http\ViewComposers\PirateComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}