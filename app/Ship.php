<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'treasure', 'pirate_id', 'displacement', 'length', 'draft', 'saltiness', 'cannons'
    ];

    /**
     * A ship belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A user has many pirates in their inventory
     */
    public function ships()
    {
        return $this->hasMany(Pirate::class);
    }

    /**
     * A ship must have a captain, which is a pirate
     */
    public function pirate()
    {
        return $this->belongsTo(Pirate::class);
    }
}
