<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait NewShip
{

    /**
     * Trait to add a new ship
     * @param $user_id
     * @return void
     */
    public function addShip($user_id)
    {
        $faker = \Faker\Factory::create();

        DB::table('ships')->insert([
            'user_id' => $user_id,
            'name' => $faker->streetName,
            'displacement' => $faker->numberBetween(10, 1000),
            'length' => $faker->numberBetween(10, 1000),
            'draft' => $faker->numberBetween(100, 1000),
            'saltiness' => 0,
            'cannons' => $faker->numberBetween(1, 10),
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

    }
}