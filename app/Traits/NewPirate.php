<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait NewPirate
{

    /**
     * Trait to create a new Pirate
     * @param $user_id
     * @return void
     */
    public function addPirate($user_id)
    {
        $faker = \Faker\Factory::create();

        $rank = ['Captain', 'First Mate', 'Second Mate', 'Sergeant-at-arms', 'Seaman', 'Cook'];

        DB::table('pirates')->insert([
            'user_id' => $user_id,
            'name' => $faker->firstName,
            'email' => $faker->email,
            'rank' => $rank[array_rand($rank, 1)],
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

    }
}