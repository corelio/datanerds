<?php

namespace App\Http\Controllers;

use App\Pirate;
use Illuminate\Http\Request;
use App\Ports2attack;
use App\Ship;

use App\Http\Requests;

class PirateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request)
    {
        $pirate = new pirate();
        $pirate->user_id = auth()->user()->id;
        $pirate->name = $request->input('pirate_name');
        $pirate->rank = $request->input('rank');
        $pirate->email = $request->input('email');
        $pirate->save();
        return redirect('home');
    }

    /**
     * Attack a port and steal its treasure
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    Public function attack(Request $request)
    {
        if(Ports2attack::where('name', '=', $request->input('port'))->first())
        {
            $port = Ports2attack::where('name', '=', $request->input('port'))->first();
            $ship = Ship::where('user_id', '=',auth()->user()->id)->first();
            $ship->treasure = $ship->treasure + $port->treasure_amount;
            $port->treasure_amount = 0;
            $port->attacked_at = date('Y-m-d H:i:s');
            $port->user_id = auth()->user()->id;
            $ship->save();
            $port->save();

        }
        return redirect('/ports');
    }
}
