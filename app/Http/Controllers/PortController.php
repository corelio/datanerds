<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PortController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * List the available ports
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listPorts()
    {
        return view('ports');
    }
}
