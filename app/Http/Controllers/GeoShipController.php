<?php

namespace App\Http\Controllers;

use App\Traits\ShipDistance;
use Illuminate\Http\Request;

class GeoShipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    use ShipDistance;

    /**
     * Collect all ships around the position given
     * Return array using the distance as keys
     * Distance is rounded
     *
     * @param Request $request
     * @return array
     */
    public function checkAround(Request $request)
    {

        $long = $request->input('long');
        $lat = $request->input('lat');

        $query = ['location' => [
            '$nearSphere' => [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        (floatval($long)),
                        (floatval($lat))
                    ]
                ],
                '$maxDistance' => 500000
            ]
        ]];

        $near = \App\GeoShip::whereRaw($query)->get(['name', 'location']);
        $result = [];
        foreach($near as $data)
        {
            $result[round($this->distance(floatval($long), floatval($lat), $data->location[0], $data->location[1], 'k'))][] = $data;
        }

        return view('shipsLocation')->with([
            'near' => [
                'lat' => $lat,
                'lng' => $long,
                'ships' => $result
            ]
        ]);

    }
}
