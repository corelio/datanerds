<?php

namespace App\Http\Controllers;

use App\Pirate;
use App\Ship;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\View\View;

class ShipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function commandeer()
    {
        //Don't show the black pearl selection if you already have a ship
        if(count(auth()->user()->ships) > 0)
            return view('home');
        return view('commandeer');
    }

    /**
     * Assign a ship to a user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function post(Request $request)
    {
        $ship = new ship();
        $ship->user_id = auth()->user()->id;
        $ship->name = $request->input('ship_name');
        $ship->displacement = 500;
        $ship->length = 300;
        $ship->draft = 800;
        $ship->saltiness = 500;
        $ship->cannons = 2;
        $ship->save();
        return view('home');
    }

    /**
     * List all user's ship
     * @return View
     */
    public function listShips()
    {
        return view('shipsList');
    }

    /**
     * Page to edit user's ships
     *
     * @param Request $request
     * @return View
     */
    public function editShip(Request $request)
    {
        if(!$request->input('ship'))
        {
            return redirect('home');
        }

        if(!Ship::find($request->input('ship')))
        {
            return redirect('ships');
        }

        $captain = DB::table('ships')
            ->leftjoin('pirates as p', 'p.id', '=', 'pirate_id')
            ->where('ships.id', $request->input('ship'))
            ->where('p.email', auth()->user()->email)
            ->select('ships.*', 'p.name as pirate_name')
            ->first();

        $isCaptain = 1;

        if(!$captain)
        {
            $ship = DB::table('ships')
                ->leftjoin('pirates as p', 'p.id', '=', 'pirate_id')
                ->where('ships.id', $request->input('ship'))
                ->select('ships.*', 'p.name as pirate_name')
                ->first();

            if(!$ship)
            {
                return redirect('ships');
            }

            $isCaptain = 0;
        }

        if($isCaptain)
        {
            return view('shipsEdit')->with([
                'ship' => $captain,
                'pirate_name' => $captain->pirate_name,
                'message' => '',
                'isCaptain' => $isCaptain,
                'crew' => $this->getCrew($request->input('ship_id'))
            ]);
        }

        return view('shipsEdit')->with([
            'ship' => $ship,
            'pirate_name' => $ship->pirate_name,
            'message' => '',
            'isCaptain' => $isCaptain,
            'crew' => $this->getCrew($request->input('ship_id'))
        ]);

    }

    /**
     * Method to update user's ships
     *
     * @param Request $request
     * @return View
     */
    public function updateShip(Request $request)
    {
        if($request->input('button') != 'Confirm')
        {
            return redirect('ships');
        }

        if(!$request->input('ship_id'))
        {
            return redirect('ships');
        }

        $own = Ship::find($request->input('ship_id'))->Where('user_id', '=', auth()->user()->id)->first();

        $captain = $this->amICaptain($request->input('ship_id'));

        if(!$own && !$captain)
        {
            return redirect('home');
        }

        $ship = Ship::where('id', $request->input('ship_id'))->first();
        $ship->displacement = $request->input('displacement');
        $ship->length = $request->input('length');
        $ship->draft = $request->input('draft');
        $ship->saltiness = $request->input('saltiness');
        $ship->cannons = $request->input('cannons');
        if($request->input('pirate_id'))
        {
            $ship->pirate_id = $request->input('pirate_id');
            $this->setCrewMember('Captain', $request->input('pirate_id'), $request->input('ship_id'));
            if($request->input('crew'))
            {
                foreach ($request->input('crew') as $key => $crew_id) {
                    $this->setCrewMember($key, $crew_id, $request->input('ship_id'));
                }
            }
        }
        if($captain)
        {
            $ship->name = $request->input('name');
        }
        $ship->save();

        $pirate_name = '';
        if(Pirate::find($request->input('pirate_id')))
        {
            $pirate_name = Pirate::find($request->input('pirate_id'))->name;
        }

        return view('shipsEdit')->with([
            'ship' => $ship,
            'message' => 'Ship Updated',
            'pirate_name' => $pirate_name,
            'isCaptain' => $this->amICaptain($request->input('ship_id')),
            'crew' => $this->getCrew($request->input('ship_id'))
        ]);
    }

    /**
     * Find out if the user is the captain of the ship
     * @param $ship_id
     * @return bool
     */
    public function amICaptain($ship_id)
    {
        return DB::table('ships')
            ->leftjoin('pirates as p', 'p.id', '=', 'pirate_id')
            ->where('ships.id', $ship_id)
            ->where('p.email', '=', auth()->user()->email)
            ->first();
    }

    /**
     * Get all the crew from a ship
     * @param $ship_id
     * @return array
     */
    public function getCrew($ship_id)
    {
        $return = [];
        $crew = Pirate::where('ship_id', $ship_id)->get();
        foreach ($crew as $pirate)
        {
            $return[$pirate->rank]['id'] = $pirate->id;
        }
        return $return;
    }

    /**
     * Define a crew to a ship and disable the old one, if exists, with the same rank
     * @param $rank
     * @param $crew_id
     * @param $ship_id
     * @return void
     */
    public function setCrewMember($rank, $crew_id, $ship_id)
    {
        //First clean possible duplicates
        DB::table('pirates')
            ->where('rank', $rank)
            ->where('ship_id', $ship_id)
            ->update(['ship_id' => 0]);

        if($crew_id)
        {
            $pirate = Pirate::where('id', $crew_id)->first();
            $pirate->ship_id = $ship_id;
            $pirate->save();
        }
    }
}
