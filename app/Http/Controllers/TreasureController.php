<?php

namespace App\Http\Controllers;

use App\Ship;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Traits\NewShip;
use App\Traits\NewPirate;

use App\Http\Requests;

class TreasureController extends Controller
{

    use NewShip;
    use NewPirate;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the treasure page
     * @return View
     */
    public function index()
    {
        return view('treasure')->with(['message' => '']);
    }

    /**
     * Use treasure function. You can buy a pirate or a ship
     * @param Request $request
     * @return View
     */
    public function useTreasure(Request $request)
    {
        if($request->input('button') == 'New Ship')
        {
            $this->addShip(auth()->user()->id);
            $ship = Ship::where('user_id', auth()->user()->id)->first();
            $ship->treasure = $ship->treasure - 10000;
            $ship->save();
            return view('treasure')->with(['message' => 'Congratulations, you have a new ship']);
        }

        if($request->input('button') == 'New Pirate')
        {
            $this->addPirate(auth()->user()->id);
            $ship = Ship::where('user_id', auth()->user()->id)->first();
            $ship->treasure = $ship->treasure - 100;
            $ship->save();
            return view('treasure')->with(['message' => 'Congratulations, you have a new pirate']);
        }

    }
}
