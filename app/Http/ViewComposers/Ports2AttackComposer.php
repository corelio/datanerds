<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Ports2attack;

class Ports2AttackComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $ports = new Ports2attack();
        $view->with('ports2attack', $ports->all());
    }
}
