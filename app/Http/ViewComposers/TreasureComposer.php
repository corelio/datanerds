<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Ship;

class TreasureComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $ship = Ship::where('user_id', '=',auth()->user()->id)->first();
            $view->with('treasure', ($ship ? $ship->treasure : 0));
        }
    }
}
