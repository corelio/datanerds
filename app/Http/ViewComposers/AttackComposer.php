<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Ports2attack;

class AttackComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $port = Ports2attack::where('user_id', '=', auth()->user()->id)->orderBy('attacked_at', 'desc')->first();
            $view->with('last_attack', ($port ? date('Y-m-d', strtotime($port->attacked_at)) : 'No attacks yet'));
        }
    }
}
