<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class UserComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
    	if (auth()->check()) {
    		$user = auth()->user();

	        $view->with('ships', $user->ships);
	        $view->with('pirates', $user->pirates);
	        $view->with('port', $user->port);

			//Captain of
			$CaptainShips = DB::table('ships')
							->leftjoin('pirates as p', 'p.id', '=', 'pirate_id')
							->where('p.email', $user->email)
							->select(
								'ships.id as id',
								'ships.name as name',
								'ships.treasure as treasure',
								'p.name as pirate_name',
								'ships.displacement as displacement',
								'ships.length as length',
								'ships.draft as draft',
								'ships.saltiness as saltiness',
								'ships.cannons as cannons'
							)
							->get();
			$return = [];
			foreach ($CaptainShips as $ship) {
				$return[] = [
					'id' => $ship->id,
					'name' => $ship->name,
					'treasure' => $ship->treasure,
					'pirate_name' => $ship->pirate_name,
					'displacement' => $ship->displacement,
					'length' => $ship->length,
					'draft' => $ship->draft,
					'saltiness' => $ship->saltiness,
					'cannons' => $ship->cannons
 				];
			}
			$view->with('captainShips', $return);
    	}
    }
}
