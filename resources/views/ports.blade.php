@extends('layouts.app')

@section('content')

    <div class="container">

        @include('layouts.header')

        <section id="game">
            <div id="game-imagery">

                <div id="port-royal">
                    <img src="/images/port-attack.jpg" alt="port royal" class="port-header">

                    <div class="inner-content">
                        <div class="row">

                            <div class="col-md-12 inventory">
                                <h2 class="text-uppercase">Ports</h2>
                                <hr>
                                <div class="icon_set">
                                    @foreach ($ports2attack as $target)
                                        <button class="port_item"  onclick="location.href='{{ url('/app/attack/') }}?port={{ $target->name }}'">
                                            <img src="images/port.png" class="icon">
                                            <span class="item-title">{{ $target->name }}</span>
                                            <span class="item-title">$ {{ $target->treasure_amount }}</span>
                                        </button>
                                    @endforeach
                                </div>

                            </div>
                            <a class="btn btn-default" href="{{ url('/home') }}"><i class="fa fa-reply" aria-hidden="true"></i> Sail back</a>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        @include('layouts.footer')

    </div>

@endsection
