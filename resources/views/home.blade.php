@extends('layouts.app')

@section('content')

    <div class="container">

        @include('layouts.header')

        <section id="game">
            <div id="game-imagery">

                <div id="port-royal">
                    <img src="images/port-royal.jpg" alt="port royal" class="port-header">

                    <div class="inner-content">
                        <div class="row">

                            <div class="col-md-3 port_stats">

                                <a href="/ports" id="btn-attack"><img src="images/btn-start-attack.png" class="attack" alt="start attack"></a>

                                <div class="stat port">
                                    <img src="images/i-ship.png" alt="icon" class="stat-icon">
                                    <p><small class="stat-label">Your Home Port:</small> <strong> {{ $port->name }}</strong></p>
                                </div>
                                <div class="stat attacks">
                                    <img src="images/i-cannon.png" alt="icon" class="stat-icon">
                                    <p><small class="stat-label">Last Port Attacked at:</small> <strong>{{ $last_attack }}</strong></p>
                                </div>
                                <div class="stat treasure">
                                    <img src="images/i-treasure.png" alt="icon" class="stat-icon">
                                    <p><small class="stat-label">Treasure Amount:</small> <strong>${{ number_format($treasure, 0, '.', ',') }}</strong></p>
                                </div>

                            </div>
                            <div class="col-md-9 inventory">
                                <h2 class="text-uppercase">Inventory:</h2>
                                <hr>
                                <h4><a href="{{ url('/ships') }}">Your Ships</a></h4>
                                <div class="icon_set ships">

                                    @foreach ($ships as $ship)
                                        @if($ship->name == 'The Black Perl')
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship->id }}">
                                                <img src="images/ship-pearl.png" class="icon">
                                                <span class="item-title">{{ $ship->name }}</span>
                                            </button>
                                        @else
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship->id }}">
                                                <img src="images/i-ship.png" class="icon">
                                                <span class="item-title">{{ $ship->name }}</span>
                                            </button>
                                        @endif
                                    @endforeach
                                </div>

                                @if(!empty($captainShips))
                                <h4><a href="{{ url('/ships') }}">Ships that you are captain</a></h4>
                                <div class="icon_set ships">

                                    @foreach ($captainShips as $ship)
                                        @if($ship['name'] == 'The Black Perl')
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#captain-ship-stats-{{ $ship['id'] }}">
                                                <img src="images/ship-pearl.png" class="icon">
                                                <span class="item-title">{{ $ship['name'] }}</span>
                                            </button>
                                        @else
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#captain-ship-stats-{{ $ship['id'] }}">
                                                <img src="images/i-ship.png" class="icon">
                                                <span class="item-title">{{ $ship['name'] }}</span>
                                            </button>
                                        @endif
                                    @endforeach
                                </div>
                                @endif

                                <h4>Your Pirates</h4>

                                <div class="icon_set ships">
                                    @foreach ($pirates as $pirate)

                                        @if ($loop->first)
                                            <button class="inventory_item pirates" data-toggle="modal" data-target="#pirate-stats">
                                                <img src="images/pirate-sparrow.png" class="icon">
                                                <span class="item-rank">{{ $pirate->rank }}</span>
                                                <span class="item-title">{{ $pirate->name }}</span>
                                            </button>
                                        @else
                                            <button class="inventory_item pirates" data-toggle="modal" data-target="#pirate-stats">
                                                <img src="images/i-pirate.png" class="icon">
                                                <span class="item-rank">{{ $pirate->rank }}</span>
                                                <span class="item-title">{{ $pirate->name }}</span>
                                            </button>
                                        @endif

                                    @endforeach
                                    @if (count($pirates) == 0)
                                        <a class="btn btn-default" href="#" data-toggle="modal" data-target="#createPirate">It looks like you need a crew. </a>
                                    @else
                                        <p><a class="btn btn-default" href="#" data-toggle="modal" data-target="#createPirate">Add more pirates </a></p>
                                    @endif
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section>

        @include('layouts.footer')

    </div>

    @foreach($ships as $ship)
    <!-- Ship Stats -->
    <div class="modal fade" id="ship-stats-{{ $ship->id }}" tabindex="-1" role="dialog" aria-labelledby="shipstats">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Ship Statistics:</h4>
                </div>
                <div class="modal-body">

                    <p class="text-center"><img src="/images/ship-pearl.png" alt="ship"></p>
                    <h2 class="text-center">{{ $ship->name }}</h2>
                    <hr class="skull">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label">Captain:</small> <strong>@if(!empty($ship->pirate)){{ $ship->pirate->name }} @else No Captain @endif</strong></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label">Displacement:</small> <strong>{{ $ship->displacement }}</strong></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label">Length:</small> <strong>{{ $ship->length }} ft.</strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label">Draft:</small> <strong>{{ $ship->draft }}</strong></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label">Crew Saltiness:</small> <strong>{{ $ship->saltiness }}</strong></p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stat">
                                <p><small class="stat-label"># of Cannons:</small> <strong>{{ $ship->cannons }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    @if(!empty($captainShips))
    @foreach($captainShips as $ship)
        <!-- Captain Ship Stats -->
        <div class="modal fade" id="captain-ship-stats-{{ $ship['id'] }}" tabindex="-1" role="dialog" aria-labelledby="captainshipstats">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ship Statistics:</h4>
                    </div>
                    <div class="modal-body">

                        <p class="text-center"><img src="/images/ship-pearl.png" alt="ship"></p>
                        <h2 class="text-center">{{ $ship['name'] }}</h2>
                        <hr class="skull">

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label">Captain:</small> <strong>{{ $ship['pirate_name'] }}</strong></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label">Displacement:</small> <strong>{{ $ship['displacement'] }}</strong></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label">Length:</small> <strong>{{ $ship['length'] }} ft.</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label">Draft:</small> <strong>{{ $ship['draft'] }}</strong></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label">Crew Saltiness:</small> <strong>{{ $ship['saltiness'] }}</strong></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="stat">
                                    <p><small class="stat-label"># of Cannons:</small> <strong>{{ $ship['cannons'] }}</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endforeach
            @endif

    <!-- Create new pirate -->
    <div class="modal fade" id="createPirate" tabindex="-1" role="dialog" aria-labelledby="createPirate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pirate Profile:</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center"><img src="images/pirate-sparrow.png" alt="pirate"></p>
                    <h2 class="text-center">Create a pirate</h2>
                    <hr class="skull">

                    <form action="/pirate/add" method="post" id="addPirate">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="key">Pirate Name:</label>
                                    <input type="text" class="form-control" name="pirate_name" id="pirate_name">
                                </div>

                                <div class="form-group">
                                    <label for="key">Pirate Rank:</label>
                                    <select class="form-control" name="rank" id="rank">
                                        <option>Select a rank</option>
                                        <option value="Captain">Captain</option>
                                        <option value="First Mate">First Mate</option>
                                        <option value="Second Mate">Second Mate</option>
                                        <option value="Sergeant-at-arms">Sergeant-at-arms</option>
                                        <option value="Seaman">Seaman</option>
                                        <option value="Cook">Cook</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="key">Email:</label>
                                    <input type="text" class="form-control" name="email" id="email">
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default btn-block"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create Pirate!</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
