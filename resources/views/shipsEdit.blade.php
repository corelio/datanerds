@extends('layouts.app')

@section('content')

    <div class="container">

        @include('layouts.header')

        <section id="game">
            <div id="game-imagery">

                <div id="port-royal">
                    <img src="/images/ships.jpg" alt="port royal" class="port-header">

                    <div class="inner-content">
                        <div class="row">

                            <div class="col-md-12 inventory">
                                <h2 class="text-uppercase">Edit Ship</h2>
                                <hr>
                                @if($message)
                                <p class="ship_message">{{ $message }}</p>
                                @endif
                                <form action="{{ url('/ships/edit?ship=') . $ship->id }}" method="post" id="editShip">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="ship_id" value="{{ $ship->id }}" />
                                <div>
                                    <p class="text-center"><img src="/images/ship-pearl.png" alt="ship"></p>
                                    <h2 class="text-center">@if($isCaptain)<input name="name" class="input_style large" type="text" value="{{ $ship->name }}"/> @else {{ $ship->name }} @endif</h2>
                                    <hr class="skull">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label">Captain:</small> <strong>
                                                            <select class="form-control pirate-selector" name="pirate_id" id="pirate_id">
                                                                <option value="">No Captain</option>
                                                                @if(!empty($pirate_list['Captain']))
                                                                    @foreach($pirate_list['Captain'] as $captain)
                                                                    <option @if($captain->id == $ship->pirate_id) selected="selected" @endif value="{{ $captain->id }}">{{ $captain->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                    </strong></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label">Displacement:</small> <strong><input name="displacement" class="input_style" type="text" value="{{ $ship->displacement }}"/></strong></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label">Length:</small> <strong><input name="length" class="input_style" type="text" value="{{ $ship->length }}"/>ft.</strong></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label">Draft:</small> <strong><input name="draft" class="input_style" type="text" value="{{ $ship->draft }}"/></strong></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label">Crew Saltiness:</small> <strong><input name="saltiness" class="input_style" type="text" value="{{ $ship->saltiness }}"/></strong></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="stat">
                                                <p><small class="stat-label"># of Cannons:</small> <strong><input id="cannons" name="cannons" class="input_style" type="text" value="{{ $ship->cannons }}"/></strong></p>
                                            </div>
                                        </div>
                                    </div>
                                    @if($isCaptain)
                                        <hr>
                                        <h3>Crew</h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="stat">
                                                    <p><small class="stat-label">First Mate:</small> <strong>
                                                            <select class="form-control pirate-selector" name="crew[first_mate]" id="first_mate">
                                                                <option value="">No First Mate</option>
                                                                @if(!empty($pirate_list['First Mate']))
                                                                    @foreach($pirate_list['First Mate'] as $pirate)
                                                                        @if(!empty($crew['First Mate']))
                                                                            <option @if($pirate->id == $crew['First Mate']['id']) selected="selected" @endif value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @else
                                                                            <option value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </strong></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="stat">
                                                    <p><small class="stat-label">Second Mate:</small> <strong>
                                                            <select class="form-control pirate-selector" name="crew[second_mate]" id="second_mate">
                                                                <option value="">No Second Mate</option>
                                                                @if(!empty($pirate_list['Second Mate']))
                                                                    @foreach($pirate_list['Second Mate'] as $pirate)
                                                                        @if(!empty($crew['Second Mate']))
                                                                            <option @if($pirate->id == $crew['Second Mate']['id']) selected="selected" @endif value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @else
                                                                            <option value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </strong></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="stat">
                                                    <p><small class="stat-label">Sergeant-at-arms:</small> <strong>
                                                            <select class="form-control pirate-selector" name="crew[Sergeant-at-arms]" id="Sergeant-at-arms">
                                                                <option value="">No Sergeant-at-arms</option>
                                                                @if(!empty($pirate_list['Sergeant-at-arms']))
                                                                    @foreach($pirate_list['Sergeant-at-arms'] as $pirate)
                                                                        @if(!empty($crew['Sergeant-at-arms']))
                                                                            <option @if($pirate->id == $crew['Sergeant-at-arms']['id']) selected="selected" @endif value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @else
                                                                            <option value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </strong></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="stat">
                                                    <p><small class="stat-label">Seaman:</small> <strong>
                                                            <select class="form-control pirate-selector" name="crew[Seaman]" id="Seaman">
                                                                <option value="">No Seaman</option>
                                                                @if(!empty($pirate_list['Seaman']))
                                                                    @foreach($pirate_list['Seaman'] as $pirate)
                                                                        @if(!empty($crew['Seaman']))
                                                                            <option @if($pirate->id == $crew['Seaman']['id']) selected="selected" @endif value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @else
                                                                            <option value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </strong></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="stat">
                                                    <p><small class="stat-label">Cook:</small> <strong>
                                                            <select class="form-control pirate-selector" name="crew[Cook]" id="Cook">
                                                                <option value="">No Cook</option>
                                                                @if(!empty($pirate_list['Cook']))
                                                                    @foreach($pirate_list['Cook'] as $pirate)
                                                                        @if(!empty($crew['Cook']))
                                                                            <option @if($pirate->id == $crew['Cook']['id']) selected="selected" @endif value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @else
                                                                            <option value="{{ $pirate->id }}">{{ $pirate->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div>
                                        <input class="btn btn-link" type="submit" name="button" value="Confirm" />
                                        <input class="btn btn-link" type="submit" name="button" value="Cancel" />
                                    </div>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        @include('layouts.footer')

    </div>

@endsection
