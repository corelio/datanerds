@extends('layouts.app')

@section('content')

<div class="container">

    @include('layouts.header')

    <section id="game">
        <div id="game-imagery">

            <div id="port-royal">

                <div class="inner-content">
                    <div class="row">

                        <div class="col-md-12 inventory">
                            <h2 class="text-uppercase">Ships Location</h2>
                            <hr>
                            <div style="width: 100%; height: 500px" id="map"></div>
                            <script>
                                function initMap() {
                                    var myLatlng = {
                                        @if(isset($near))
                                        lat: {{ $near['lat'] }},
                                        lng: {{ $near['lng'] }}
                                        @else
                                        lat: 47,
                                        lng: -148
                                        @endif
                                    };

                                    var pirate = {
                                        url: '/images/pirate.ico',
                                    };
                                    var other = {
                                        url: '/images/pirate_other.ico',
                                        labelOrigin: new google.maps.Point(20,35)
                                    };

                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: @if(isset($near)) 5 @else 4 @endif,
                                        center: myLatlng
                                    });

                                    var markersArray = [];

                                    var marker = new google.maps.Marker({
                                        position: myLatlng,
                                        icon: pirate,
                                        map: map,
                                    });

                                    markersArray.push(marker);

                                    @if(isset($near))
                                     @foreach($near['ships'] as $distance => $ship)
                                        var markerOther = new google.maps.Marker({
                                            position: {lat: {{ $ship[0]->location[1] }}, lng: {{ $ship[0]->location[0] }}},
                                            icon: other,
                                            map: map,
                                            label: {
                                                text: '{{ $ship[0]->name }}' + ' [' + '{{ $distance }}' + ' km]',
                                                color: '#6666ff'
                                            }
                                        });
                                        markersArray.push(markerOther);
                                     @endforeach
                                    @endif

                                    map.addListener('click', function (event) {
                                         if(markersArray.length > 1)
                                         {
                                             for(var i = 1; i < markersArray.length; i++)
                                             {
                                                 markersArray[i].setMap(null);
                                             }
                                             map.setZoom(4);
                                         }
                                        marker.setPosition(event.latLng);
                                        document.getElementById('lat').value = event.latLng.lat();
                                        document.getElementById('long').value = event.latLng.lng();
                                        document.getElementById('findShipButton').disabled = false;
                                    });

                                    var boundariesCoordinates = [
                                        {lat: 36.9, lng: -122.9},
                                        {lat: 36.9, lng: -170.1},
                                        {lat: 55.1, lng: -170.1},
                                        {lat: 55.1, lng: -122.9},
                                        {lat: 36.9, lng: -122.9}
                                    ];

                                    var boundariesPath = new google.maps.Polyline({
                                        path: boundariesCoordinates,
                                        geodesic: true,
                                        strokeColor: 'black',
                                        strokeOpacity: 0.3,
                                        strokeWeight: 2
                                    });

                                    boundariesPath.setMap(map);

                                }
                            </script>
                            <script async defer
                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzy4xKW78CUxjzP12Kv4V_ywAXS3pHbT0&callback=initMap">
                            </script>
                            <form action="{{ url('/ships/near') }}" method="post" id="editShip">
                                {{ csrf_field() }}
                                <input type="hidden" name="lat" id="lat" />
                                <input type="hidden" name="long" id="long" />
                                <div class="row">
                                    <div class="col-lg-2">
                                        <a class="btn btn-default" href="{{ url('/home') }}"><i class="fa fa-reply" aria-hidden="true"></i> Sail back</a>
                                    </div>
                                    <div class="col-lg-offset-10">
                                        <input disabled="disabled" class="btn btn-link" type="submit" name="button" id="findShipButton" value="Find Ships" />
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>

    @include('layouts.footer')

</div>

@endsection
