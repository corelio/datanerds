@extends('layouts.app')

@section('content')

    <div class="container">

        @include('layouts.header')

        <section id="game">
            <div id="game-imagery">

                <div id="port-royal">
                    <img src="/images/ships.jpg" alt="port royal" class="port-header">

                    <div class="inner-content">
                        <div class="row">

                            <div class="col-md-12 inventory">
                                <h2 class="text-uppercase">Ships</h2></a>
                                <hr>
                                <div class="icon_set ships">

                                    @foreach ($ships as $ship)
                                        @if($ship->name == 'The Black Perl')
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship->id }}" onclick="location.href='{{ url('/ships/edit?ship=') }}{{ $ship->id }}'">
                                                <img src="images/ship-pearl.png" class="icon">
                                                <span class="item-title">{{ $ship->name }}</span>
                                            </button>
                                        @else
                                            <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship->id }}" onclick="location.href='{{ url('/ships/edit?ship=') }}{{ $ship->id }}'">
                                                <img src="images/i-ship.png" class="icon">
                                                <span class="item-title">{{ $ship->name }}</span>
                                            </button>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            @if(!empty($captainShips))
                                <div class="col-md-12 inventory">
                                    <h2 class="text-uppercase">Captain of Ships</h2></a>
                                    <hr>
                                    <div class="icon_set ships">

                                        @foreach ($captainShips as $ship)
                                            @if($ship['name'] == 'The Black Perl')
                                                <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship['id'] }}" onclick="location.href='{{ url('/ships/edit?ship=') }}{{ $ship['id'] }}'">
                                                    <img src="images/ship-pearl.png" class="icon">
                                                    <span class="item-title">{{ $ship['name'] }}</span>
                                                </button>
                                            @else
                                                <button class="inventory_item ship" data-toggle="modal" data-target="#ship-stats-{{ $ship['id'] }}" onclick="location.href='{{ url('/ships/edit?ship=') }}{{ $ship['id'] }}'">
                                                    <img src="images/i-ship.png" class="icon">
                                                    <span class="item-title">{{ $ship['name'] }}</span>
                                                </button>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            <a class="btn btn-default" href="{{ url('/home') }}"><i class="fa fa-reply" aria-hidden="true"></i> Sail back</a>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        @include('layouts.footer')

    </div>

@endsection
