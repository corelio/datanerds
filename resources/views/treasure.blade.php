@extends('layouts.app')

@section('content')

    <div class="container">

        @include('layouts.header')

        <section id="game">
            <div id="game-imagery">

                <div id="port-royal">
                    <img src="/images/treasure.jpg" alt="port royal" class="port-header">

                    <div class="inner-content">
                        <div class="row">

                            <div class="col-md-12 inventory">
                                <h2 class="text-uppercase">Treasure</h2>
                                <hr>
                                @if($message)
                                    <p class="ship_message">{{ $message }}</p>
                                @endif
                                <form action="{{ url('/treasure') }}" method="post" id="useTreasure">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <h3 class="text-center">Total amount: ${{ number_format($treasure, 0, '.', ',') }}</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-lg-offset-1">
                                            <div class="text-center">
                                                <img src="images/pirate-sparrow.png" class="icon">
                                                <span class="item-title">Pirate price $100</span>
                                                <hr>
                                                <input @if($treasure < 100) disabled @endif class="btn btn-link buy-button" type="submit" name="button" value="New Pirate" />
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-lg-offset-2">
                                            <div class="text-center">
                                                <img src="images/ship-pearl.png" class="icon">
                                                <span class="item-title">Ship price $10,000</span>
                                                <hr>
                                                <input @if($treasure < 10000) disabled @endif class="btn btn-link buy-button" type="submit" name="button" value="New Ship" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <a class="btn btn-default" href="{{ url('/home') }}"><i class="fa fa-reply" aria-hidden="true"></i> Sail back</a>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        @include('layouts.footer')

    </div>

@endsection
